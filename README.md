# GLInfo2

Outils communs pour le projet de Génie Logiciel des INFO2, Promotion 2021
Pour Java, préférez une éthique de programmation selon ce type de guide : https://google.github.io/styleguide/javaguide.html
De manière générale, programmez en anglais (noms des variables, classes, méthodes...), commentez votre code, au minimum fournissez une JavaDoc (@author, @brief, @in et @out) pour chaque classe et méthode.

Ces outils seront ceux potentiellement utilisés par différentes équipes, comme les objets prédéfinis en JavaFX ou les vues en MySQL.